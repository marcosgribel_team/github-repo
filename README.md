## Challenge Description ##

Your goal is to create an android app where the user can search for repositories on github by keywords as well as any qualifiers using the api: https://developer.github.com/v3/search/#search-repositories and display the results in a list.

### Get Started ###

* [Android Studio](https://developer.android.com/studio/index.html)
* [Java 1.8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
* [Configurar o plugin do Kotlin no Android Studio](https://blog.jetbrains.com/kotlin/2013/08/working-with-kotlin-in-android-studio/)



### Project Structure

This project leverages on several modern approaches to build Android applications :

- 100% written in Kotlin programming language.
- [MVVM Architecture](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel). powered by Android Architecture Components
- All packages is organized following the practice called "package by feature", that way is easily to modularize the project when it expand and also in case the teams who works based in features is a good strategy to modularize it by team.
- Based on [Material Design](https://material.io/design/) guideline.


## Dependencies
- Koin
- RxJava/RxAndroid
- Architecture Components
    - Paging Library
	- Live Data
	- ViewModel
- Retroit
- Glide
- Lottie
- Robolectric
...[Check All](https://bitbucket.org/marcosgribel_team/github-repo/src/51caa04c15514171b68bc4285b94b643db464aee/dependencies.gradle?at=master)
