package com.marcosgribel.github_repo_app.repo.model.interactor

import com.marcosgribel.github_repo_app.core.domain.Repo
import com.marcosgribel.github_repo_app.repo.model.service.RepoService
import io.reactivex.Single

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface RepoInteractor {

    fun search(
            query: String,
            page: Int,
            sizePerPage: Int
    ): Single<List<Repo>>

}

class RepoInteractorImpl(
        private val repoService: RepoService
) : RepoInteractor {

    override fun search(query: String, page: Int, sizePerPage: Int): Single<List<Repo>> {
        return repoService.search(query, page, sizePerPage).map {
            it.items
        }
    }
}