package com.marcosgribel.github_repo_app.repo.view.adapter

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.view.ViewGroup
import com.marcosgribel.github_repo_app.core.domain.Repo

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class RepoAdapter: PagedListAdapter<Repo, RepoViewHolder>(COMPARATOR_ITEM_CALLBACK) {

    companion object {

        private val COMPARATOR_ITEM_CALLBACK = object : DiffUtil.ItemCallback<Repo>() {

            override fun areItemsTheSame(oldItem: Repo, newItem: Repo): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Repo, newItem: Repo): Boolean = oldItem.name == newItem.name
        }
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        return RepoViewHolder.inflate(parent)
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        val data = getItem(position)
        holder.bindData(data)
    }

}