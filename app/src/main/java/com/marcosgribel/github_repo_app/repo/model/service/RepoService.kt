package com.marcosgribel.github_repo_app.repo.model.service

import com.marcosgribel.github_repo_app.core.domain.RepoResponse
import com.marcosgribel.github_repo_app.core.model.BaseService
import io.reactivex.Single

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface RepoService {


    fun search(
            query: String,
            page: Int,
            sizePerPage: Int
    ): Single<RepoResponse>

}


class RepoServiceImpl(private val repoApi: RepoApi) : BaseService(), RepoService {

    override fun search(query: String, page: Int, sizePerPage: Int): Single<RepoResponse> {
        return repoApi.search(query, page, sizePerPage)
    }

}
