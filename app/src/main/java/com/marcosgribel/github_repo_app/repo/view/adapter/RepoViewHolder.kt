package com.marcosgribel.github_repo_app.repo.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcosgribel.github_repo_app.R
import com.marcosgribel.github_repo_app.core.domain.Repo
import com.marcosgribel.github_repo_app.core.extensions.loadUrl
import kotlinx.android.synthetic.main.item_list_repo.view.*

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class RepoViewHolder(val view: View) : RecyclerView.ViewHolder(view) {


    companion object {

        fun inflate(parent: ViewGroup): RepoViewHolder {
            val view =  LayoutInflater.from(parent.context).inflate(R.layout.item_list_repo, parent, false)
            return RepoViewHolder(view)
        }

    }


    /*

     */
    fun bindData(data: Repo?){


        if(data != null){
            view.img_user_picture_repo.loadUrl(data.owner.avatarUrl, R.drawable.ic_github_logo)
            view.text_title_repo.text = data.name
            view.text_description_repo.text = data.description

            view.text_forks_count_repo.text = data.forksCount.toString()
            view.text_stargazers_count_repo.text = data.stargazersCount.toString()
            view.text_watchers_count_repo.text = data.watchersCount.toString()

        }

    }

}