package com.marcosgribel.github_repo_app.repo.model.datasource

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.marcosgribel.github_repo_app.core.domain.Repo
import com.marcosgribel.github_repo_app.core.domain.enumerator.Status
import com.marcosgribel.github_repo_app.core.rx.SchedulerProvider
import com.marcosgribel.github_repo_app.repo.model.RepoDataSource
import com.marcosgribel.github_repo_app.repo.model.interactor.RepoInteractor
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class RepoDataSourceFactory(private val interactor: RepoInteractor,
                            private val scheduler: SchedulerProvider,
                            private val compositeDisposable: CompositeDisposable,
                            private val status: MutableLiveData<Status>
) : DataSource.Factory<Int, Repo> {

    var dataSource = MutableLiveData<RepoDataSource>()
    var searchQuery = MutableLiveData<String>()


    override fun create(): DataSource<Int, Repo> {
        val strQuery = searchQuery.value
        val repoDataSource = RepoDataSource(interactor, scheduler, compositeDisposable, strQuery, status)
        dataSource.postValue(repoDataSource)

        return repoDataSource
    }


}