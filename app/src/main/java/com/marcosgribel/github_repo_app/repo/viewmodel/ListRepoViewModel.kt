package com.marcosgribel.github_repo_app.repo.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.marcosgribel.github_repo_app.core.domain.Repo
import com.marcosgribel.github_repo_app.core.domain.enumerator.Status
import com.marcosgribel.github_repo_app.core.rx.SchedulerProvider
import com.marcosgribel.github_repo_app.repo.model.RepoDataSource
import com.marcosgribel.github_repo_app.repo.model.datasource.RepoDataSourceFactory
import com.marcosgribel.github_repo_app.repo.model.interactor.RepoInteractor
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Predicate
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ListRepoViewModel(
        private val interactor: RepoInteractor,
        private val scheduler: SchedulerProvider,
        private val compositeDisposable: CompositeDisposable
) : ViewModel() {


    /*
        Variables
     */


    private var searchSubject: PublishSubject<String>? = null
    private var searchDisposable: Disposable? = null

    private val pageSize = 20
    private var pagedListConfig: PagedList.Config


    var listOfRepo: LiveData<PagedList<Repo>> = MutableLiveData<PagedList<Repo>>()
    val status = MutableLiveData<Status>()


    private val dataSourceFactory = RepoDataSourceFactory(interactor,scheduler, compositeDisposable, status)




    init {

        val tag = ListRepoViewModel::class.java.simpleName.toString()
        Timber.tag(tag)

        pagedListConfig = PagedList.Config.Builder()
                .setInitialLoadSizeHint(pageSize)
                .setPageSize(pageSize)
                .setPrefetchDistance(pageSize / 2)
                .setEnablePlaceholders(false)
                .build()

        listOfRepo = LivePagedListBuilder<Int, Repo>(dataSourceFactory, pagedListConfig)
                .build()

    }


    /*

        Methods

     */



    fun onSearchStart() {

        searchSubject = PublishSubject.create<String>()
        searchDisposable = searchSubject!!
                .debounce(300, TimeUnit.MILLISECONDS)
                .filter(Predicate {
                    return@Predicate (it.isNotEmpty() && it.isNotBlank())
                })
                .distinctUntilChanged()
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.mainThread())
                .subscribe ({
                    dataSourceFactory.dataSource.value!!.invalidate()
                    dataSourceFactory.searchQuery.postValue(it)
                }, {
                    Timber.e(it)
                })

    }


    fun onSearch(query: String) {
        searchSubject?.onNext(query)
    }


    fun onSearchComplete() {
        if (searchDisposable != null && searchDisposable!!.isDisposed) {
            searchDisposable!!.dispose()
            searchSubject = null
        }
    }

    fun onSearchClose(){
        dataSourceFactory.dataSource.value?.invalidate()
    }

    fun onRetry(){
        dataSourceFactory.dataSource.value?.retryErrorCallback?.retry()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }


}