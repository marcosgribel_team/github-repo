package com.marcosgribel.github_repo_app.repo.model

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.ItemKeyedDataSource
import com.marcosgribel.github_repo_app.core.domain.Repo
import com.marcosgribel.github_repo_app.core.domain.callback.RetryErrorCallback
import com.marcosgribel.github_repo_app.core.domain.enumerator.Status
import com.marcosgribel.github_repo_app.core.rx.SchedulerProvider
import com.marcosgribel.github_repo_app.repo.model.interactor.RepoInteractor
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */

class RepoDataSource(private val interactor: RepoInteractor,
                     private val scheduler: SchedulerProvider,
                     private val compositeDisposable: CompositeDisposable,
                     private val searchQuery: String?,
                     private val status: MutableLiveData<Status>

) : ItemKeyedDataSource<Int, Repo>() {

    interface RepoDataSourceCallback {

        fun onSuccess(list: List<Repo>)

        fun onFailure(throwable: Throwable)

    }


    private var nextPage = 1
    val currentPage: Int
        get() = nextPage


    var retryErrorCallback: RetryErrorCallback? = null

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Repo>) {

    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Repo>) {

        onLoad(searchQuery, nextPage, params.requestedLoadSize, object : RepoDataSourceCallback {
            override fun onSuccess(list: List<Repo>) {
                callback.onResult(list)
            }

            override fun onFailure(throwable: Throwable) {
                attachRetryErrorCallback {
                    loadInitial(params, callback)
                }
            }
        })

    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Repo>) {

        onLoad(searchQuery, nextPage, params.requestedLoadSize, object : RepoDataSourceCallback {
            override fun onSuccess(list: List<Repo>) {
                callback.onResult(list)
            }

            override fun onFailure(throwable: Throwable) {
                attachRetryErrorCallback {
                    loadAfter(params, callback)
                }

            }
        })

    }

    override fun getKey(item: Repo): Int = nextPage



    fun onLoad(requestedQuery: String?, requestedPage: Int, requestedLoadSize: Int, callback: RepoDataSourceCallback) {

        if (requestedQuery.isNullOrEmpty()) {
            status.postValue(Status.EMPTY)
            return
        }


        status.postValue(Status.LOADING)

        var disposable = interactor.search(
                requestedQuery!!,
                requestedPage,
                requestedLoadSize)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.mainThread()).subscribe({
                    this.onSuccess()
                    callback.onSuccess(it)

                }, {
                    onFailure(it)
                    callback.onFailure(it)
                })

        compositeDisposable.addAll(disposable)

    }


    fun onSuccess() {
        nextPage++
        status.postValue(Status.SUCCESS)
    }

    fun onFailure(throwable: Throwable) {
        Timber.e(throwable)
        status.postValue(Status.ERROR)
    }


    private fun attachRetryErrorCallback(callback: () -> Unit) {
        retryErrorCallback = object : RetryErrorCallback {
            override fun retry() {
                callback()
            }
        }
    }

}