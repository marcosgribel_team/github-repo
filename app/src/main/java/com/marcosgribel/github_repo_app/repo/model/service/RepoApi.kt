package com.marcosgribel.github_repo_app.repo.model.service

import com.marcosgribel.github_repo_app.core.domain.RepoResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface RepoApi {


    @GET("search/repositories?sort=stars")
    fun search(
            @Query("q") query: String,
            @Query("page") page: Int,
            @Query("per_page") sizePerPage: Int) : Single<RepoResponse>

}