package com.marcosgribel.github_repo_app.repo.view.ui

import android.arch.lifecycle.Observer
import android.arch.paging.PagedList
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.marcosgribel.github_repo_app.R
import com.marcosgribel.github_repo_app.core.domain.Repo
import com.marcosgribel.github_repo_app.core.domain.enumerator.Status
import com.marcosgribel.github_repo_app.repo.view.adapter.RepoAdapter
import com.marcosgribel.github_repo_app.repo.viewmodel.ListRepoViewModel
import kotlinx.android.synthetic.main.activity_list_repo.*
import org.koin.android.ext.android.inject

class ListRepoActivity : AppCompatActivity() {

    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, ListRepoActivity::class.java)
        }

    }


    val viewModel: ListRepoViewModel by inject()

    private val adapter = RepoAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_repo)

        initialize()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_list_repo, menu)

        configSearchView(menu)

        return super.onCreateOptionsMenu(menu)
    }




    private fun initialize() {
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_list_of_repo.layoutManager = linearLayoutManager
        recycler_list_of_repo.setHasFixedSize(true)
        recycler_list_of_repo.adapter = adapter

        viewModel.status.observe(this, onStatusObserver())

        onEmpty()
    }


    private fun configSearchView(menu: Menu?){
        var menuSearch = menu?.findItem(R.id.action_search)
        menuSearch?.setOnActionExpandListener(onSearchMenuExpandListener())

        val searchView = menuSearch!!.actionView as SearchView
        searchView?.setOnQueryTextListener(onSearchQueryTextListener())
        searchView?.setOnCloseListener(onSearchCloseListener())
    }

    private fun onSearchCloseListener(): SearchView.OnCloseListener {
        return SearchView.OnCloseListener {

           onSearchClose()

            true
        }
    }

    private fun onSearchQueryTextListener(): SearchView.OnQueryTextListener {
        return object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                onSearch(newText)
                return true
            }
        }
    }

    private fun onSearchMenuExpandListener(): MenuItem.OnActionExpandListener {
        return object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                onSearchStart()
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                onSearchComplete()
                return true
            }
        }
    }

    private fun onPagedListRepoObserver(): Observer<PagedList<Repo>> {
        return Observer { t ->
            adapter.submitList(t)
        }
    }

    fun onSearchStart() {
        viewModel.onSearchStart()
        viewModel.listOfRepo.observe(this, onPagedListRepoObserver())
    }

    fun onSearch(query : String){
        viewModel.onSearch(query)
    }

    fun onSearchComplete() {
        viewModel.onSearchComplete()
        viewModel.listOfRepo.removeObserver(onPagedListRepoObserver())
    }

    fun onSearchClose() {
        viewModel.onSearchClose()
        onEmpty()
    }


    fun onStatusObserver(): Observer<Status> {
        return Observer { t ->
            when (t) {
                Status.LOADING -> onLoading()
                Status.EMPTY -> onEmpty()
                Status.SUCCESS -> onSuccess()
                Status.ERROR -> onError()
            }
        }

    }

    fun onLoading() {
        progress_bar.visibility = View.VISIBLE
    }

    fun onEmpty() {
        progress_bar.visibility = View.GONE
        recycler_list_of_repo.visibility = View.GONE
        layout_empty_list_repo.visibility = View.VISIBLE
    }

    fun onSuccess() {
        progress_bar.visibility = View.GONE
        layout_empty_list_repo.visibility = View.GONE
        recycler_list_of_repo.visibility = View.VISIBLE
    }

    fun onError() {
        progress_bar.visibility = View.GONE

        var message = getString(R.string.message_could_not_load_try_again)
        var view = findViewById<View>(R.id.constraint_layout_list_of_repo)
        val snackBar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)

        snackBar.setAction(R.string.retry) {
            snackBar.dismiss()
            viewModel.onRetry()
        }

        snackBar.show()

    }



}
