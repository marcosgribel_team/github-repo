package com.marcosgribel.github_repo_app.core.domain

import com.squareup.moshi.Json


data class Owner(
        @field:Json(name = "login")
        val login: String? = null,

        @field:Json(name = "avatar_url")
        val avatarUrl: String? = null,

        @field:Json(name = "id")
        val id: Int? = null
)