package com.marcosgribel.github_repo_app.core.model

import com.marcosgribel.github_repo_app.core.di.NetworkModule

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
open class BaseService {

    internal fun <T> get(network: NetworkModule, clazz: Class<T>): T {
        return network.provideRetrofit().create(clazz)
    }

}