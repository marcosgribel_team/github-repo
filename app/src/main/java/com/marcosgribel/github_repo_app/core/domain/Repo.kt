package com.marcosgribel.github_repo_app.core.domain

import com.squareup.moshi.Json


data class Repo(

	@field:Json(name="owner")
	val owner: Owner,

	@field:Json(name="topics")
	val topics: List<String?>? = null,

	@field:Json(name="description")
	val description: String,

	@field:Json(name="created_at")
	val createdAt: String? = null,

	@field:Json(name="full_name")
	val fullName: String? = null,

	@field:Json(name="updated_at")
	val updatedAt: String? = null,

	@field:Json(name="html_url")
	val htmlUrl: String? = null,

	@field:Json(name="name")
	val name: String,

	@field:Json(name="id")
	val id: Int,

	@field:Json(name="watchers_count")
	val watchersCount: Int,

	@field:Json(name="stargazers_count")
	val stargazersCount: Int,

	@field:Json(name="forks_count")
	val forksCount: Int
)