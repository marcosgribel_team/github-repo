package com.marcosgribel.github_repo_app.core.domain

import com.squareup.moshi.Json

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
data class RepoResponse(
        @Json(name = "items")
        val items: List<Repo> = arrayListOf(),
        @Json(name = "total_count")
        val totalCount: Int
)