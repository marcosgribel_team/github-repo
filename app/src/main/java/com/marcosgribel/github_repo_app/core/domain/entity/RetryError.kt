package com.marcosgribel.github_repo_app.core.domain.entity

import com.marcosgribel.github_repo_app.core.domain.callback.RetryErrorCallback

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */

data class RetryError(val message: String, val retryErrorCallBack: RetryErrorCallback)