package com.marcosgribel.github_repo_app.core.di

import com.marcosgribel.github_repo_app.core.model.BaseService
import com.marcosgribel.github_repo_app.core.rx.AppScheduler
import com.marcosgribel.github_repo_app.core.rx.SchedulerProvider
import com.marcosgribel.github_repo_app.repo.model.RepoDataSource
import com.marcosgribel.github_repo_app.repo.model.datasource.RepoDataSourceFactory
import com.marcosgribel.github_repo_app.repo.model.interactor.RepoInteractor
import com.marcosgribel.github_repo_app.repo.model.interactor.RepoInteractorImpl
import com.marcosgribel.github_repo_app.repo.model.service.RepoApi
import com.marcosgribel.github_repo_app.repo.model.service.RepoService
import com.marcosgribel.github_repo_app.repo.model.service.RepoServiceImpl
import com.marcosgribel.github_repo_app.repo.viewmodel.ListRepoViewModel
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */


val coreModule = applicationContext {

    bean { NetworkModule() }
    bean { BaseService() }
}

val rxModule = applicationContext {

    bean { AppScheduler() as SchedulerProvider }

}

val repoModule = applicationContext {

    val disposable = CompositeDisposable()

    bean { }

    // model
    bean {
        RepoServiceImpl((get() as BaseService).get(get(), RepoApi::class.java)) as RepoService
    }

    bean { RepoInteractorImpl(get()) as RepoInteractor }

    // viewmodel
    viewModel {
        ListRepoViewModel(
                get(),
                get(),
                disposable
        )
    }

}

val appModule = listOf(coreModule, rxModule, repoModule)