package com.marcosgribel.github_repo_app.core.di

import com.marcosgribel.github_repo_app.core.rx.AppSchedulerTest
import com.marcosgribel.github_repo_app.core.rx.SchedulerProvider
import com.marcosgribel.github_repo_app.repo.model.interactor.RepoInteractorTest
import com.marcosgribel.github_repo_app.repo.model.service.RepoServiceTest
import com.marcosgribel.github_repo_app.repo.viewmodel.ListRepoViewModel
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */


val rxModule = applicationContext {

    bean { AppSchedulerTest() as SchedulerProvider }

}

val repoModule = applicationContext {

    bean { RepoServiceTest().provide() }

    bean { RepoInteractorTest().provide(get()) }


    // viewmodel
    viewModel {
        ListRepoViewModel(
                get(),
                get(),
                CompositeDisposable()
        )
    }
}

val appModuleTest = listOf(rxModule, repoModule)