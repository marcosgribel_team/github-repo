package com.marcosgribel.github_repo_app.repo.model

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import com.marcosgribel.github_repo_app.core.domain.Repo
import com.marcosgribel.github_repo_app.core.domain.enumerator.Status
import com.marcosgribel.github_repo_app.core.rx.AppSchedulerTest
import com.marcosgribel.github_repo_app.core.rx.SchedulerProvider
import com.marcosgribel.github_repo_app.repo.model.interactor.RepoInteractor
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.*
import org.mockito.Mockito.*

/**
 * Created by marcosgribel.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */
class RepoDataSourceTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()


    @Mock
    private lateinit var callbackMock: RepoDataSource.RepoDataSourceCallback


    @Mock
    private lateinit var interactorMock: RepoInteractor


    private val scheduler: SchedulerProvider = AppSchedulerTest()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val searchQuery: String? = null
    private val status: MutableLiveData<Status> = MutableLiveData()


    private lateinit var dataSource: RepoDataSource


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        val _dataSource = RepoDataSource(interactorMock, scheduler, compositeDisposable, searchQuery, status)
        dataSource = spy(_dataSource)

    }


    @Test
    fun assertOnSuccess() {
        dataSource.onSuccess()
        Assert.assertEquals(dataSource.currentPage, 2)
        Assert.assertEquals(Status.SUCCESS, status.value)
    }

    @Test
    fun assertOnFailure() {
        dataSource.onFailure(mock(Throwable::class.java))
        Assert.assertEquals(Status.ERROR, status.value)

    }



    @Test
    fun assertOnLoadSuccess_NotEmpty() {

        val strQuery = "anyString"
        val fakePage = 1
        val fakePageSize = 10


        `when`(interactorMock.search(strQuery, fakePage, fakePageSize))
                .thenReturn(Single.just(arrayListOf()))

        dataSource.onLoad(strQuery, fakePage, fakePageSize, callbackMock)

        Assert.assertEquals(Status.SUCCESS, status.value)

        Mockito.verify(callbackMock).onSuccess(ArgumentMatchers.anyList())

        Mockito.verify(dataSource).onSuccess()

    }

    @Test
    fun assertOnLoadSuccess_Empty() {

        `when`(interactorMock.search(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt()))
                .thenReturn(Single.just(ArgumentMatchers.anyList()))

        dataSource.onLoad(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), callbackMock)

        Assert.assertEquals(Status.EMPTY, status.value)

    }

    @Test
    fun assertOnLoadFailure() {

        val strQuery = "anyString"
        val fakePage = 1
        val fakePageSize = 10

        val fakeException = mock(Exception::class.java)

        `when`(interactorMock.search(strQuery, fakePage, fakePageSize))
                .thenReturn(Single.error(fakeException))

        dataSource.onLoad(strQuery, fakePage, fakePageSize, callbackMock)

        Assert.assertEquals(Status.ERROR, status.value)

        Mockito.verify(callbackMock).onFailure(fakeException)

        Mockito.verify(dataSource).onFailure(fakeException)

    }

}