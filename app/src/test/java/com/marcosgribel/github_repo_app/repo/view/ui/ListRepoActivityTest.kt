package com.marcosgribel.github_repo_app.repo.view.ui

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.view.View
import com.marcosgribel.github_repo_app.core.di.appModuleTest
import com.marcosgribel.github_repo_app.core.domain.enumerator.Status
import junit.framework.Assert.assertEquals
import kotlinx.android.synthetic.main.activity_list_repo.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.standalone.StandAloneContext
import org.koin.test.KoinTest
import org.mockito.Mockito.spy
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

/**
 * Created by marcosgribel.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */

@RunWith(RobolectricTestRunner::class)
class ListRepoActivityTest : KoinTest {


    @get:Rule
    val rule = InstantTaskExecutorRule()

//    private val viewModel: ListRepoViewModel by inject()

    private lateinit var listRepoActivity: ListRepoActivity


    @Before
    fun setUp(){

        MockitoAnnotations.initMocks(this)

        val activity = Robolectric.buildActivity(ListRepoActivity::class.java).create().visible().get()
        listRepoActivity = spy(activity)

        StandAloneContext.loadKoinModules(appModuleTest)
    }

    @After
    fun setDown(){
        StandAloneContext.closeKoin()
    }



    @Test
    fun assertOnStatusObserver(){
        val observer = listRepoActivity.onStatusObserver()

        listRepoActivity.viewModel.status.observeForever(observer)

        listRepoActivity.viewModel.status.postValue(Status.SUCCESS)
        verify(listRepoActivity).onSuccess()

        listRepoActivity.viewModel.status.postValue(Status.EMPTY)
        verify(listRepoActivity).onEmpty()

        listRepoActivity.viewModel.status.postValue(Status.ERROR)
        verify(listRepoActivity).onError()

        listRepoActivity.viewModel.status.postValue(Status.LOADING)
        verify(listRepoActivity).onLoading()

    }



    @Test
    fun assertOnLoading() {

        listRepoActivity.onLoading()

        assertEquals(View.VISIBLE, listRepoActivity.progress_bar.visibility)
    }


    @Test
    fun assertOnEmpty() {

        listRepoActivity.onEmpty()

        assertEquals(View.GONE, listRepoActivity.progress_bar.visibility)
        assertEquals(View.GONE, listRepoActivity.recycler_list_of_repo.visibility)
        assertEquals(View.VISIBLE, listRepoActivity.layout_empty_list_repo.visibility)

    }

    @Test
    fun assertOnSuccess() {

        listRepoActivity.onSuccess()

        assertEquals(View.GONE, listRepoActivity.progress_bar.visibility)
        assertEquals(View.GONE, listRepoActivity.layout_empty_list_repo.visibility )
        assertEquals(View.VISIBLE, listRepoActivity.recycler_list_of_repo.visibility)

    }


    @Test
    fun assertOnError(){
        listRepoActivity.onError()

        assertEquals(View.GONE, listRepoActivity.progress_bar.visibility)

    }


}