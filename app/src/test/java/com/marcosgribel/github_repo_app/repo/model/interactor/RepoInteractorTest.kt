package com.marcosgribel.github_repo_app.repo.model.interactor

import com.marcosgribel.github_repo_app.repo.model.service.RepoService

/**
 * Created by marcosgribel
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */
class RepoInteractorTest{

    fun provide(service: RepoService): RepoInteractor = RepoInteractorImpl(service)

}