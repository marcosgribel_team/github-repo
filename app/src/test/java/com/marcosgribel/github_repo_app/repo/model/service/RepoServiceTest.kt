package com.marcosgribel.github_repo_app.repo.model.service

import com.marcosgribel.github_repo_app.AppMockHelper
import com.marcosgribel.github_repo_app.core.domain.RepoResponse
import com.squareup.moshi.Moshi
import io.reactivex.Single
import org.mockito.ArgumentMatchers
import org.mockito.Mockito

/**
 * Created by marcosgribel
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */
class RepoServiceTest {

    companion object {

        private const val REPOSITORIES_PAGE_1_JSON = "repositories_page_1.json"
        private const val REPOSITORIES_PAGE_2_JSON = "repositories_page_2.json"

        private const val QUERY_FAKE = "query"
        private const val PAGE_SIZE_FAKE = 10
    }


    /*
        Provide mock instance
     */
    fun provide(): RepoService {


        val repoServiceMock = Mockito.mock(RepoServiceImpl::class.java)
        Mockito.`when`(repoServiceMock.search(QUERY_FAKE, 1, PAGE_SIZE_FAKE))
                .thenReturn(Single.just(fake_repo_page_1()))

        Mockito.`when`(repoServiceMock.search(QUERY_FAKE, 2, PAGE_SIZE_FAKE)).thenReturn(Single.just(fake_repo_page_2()))

        return repoServiceMock
    }


    fun fake_repo_page_1(): RepoResponse {
        val inputStream = this::class.java.classLoader.getResourceAsStream(REPOSITORIES_PAGE_1_JSON)
        val strJson = AppMockHelper.readStream(inputStream)

        val adapter = Moshi.Builder().build().adapter(RepoResponse::class.java)
        return adapter.fromJson(strJson)!!
    }

    fun fake_repo_page_2(): RepoResponse {
        val inputStream = this::class.java.classLoader.getResourceAsStream(REPOSITORIES_PAGE_2_JSON)
        val strJson = AppMockHelper.readStream(inputStream)

        val adapter = Moshi.Builder().build().adapter(RepoResponse::class.java)
        return adapter.fromJson(strJson)!!
    }


}